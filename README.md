# Voice-Controlled Robot Web Application

## Overview

This is a web application for voice control of a robot using the Streamlit framework. The application is designed to recognize voice commands in the Russian language and control the robot accordingly. Currently, it supports five commands: move forward, move backward, stop, turn to the left, and turn to the right.

## Features

- Record voice command input from the user.
- Process the recorded audio and predict the corresponding robot command.
- Display the predicted command to the user.

## Prerequisites

Make sure you have the following dependencies installed:

- [Streamlit](https://streamlit.io/)
- [Sounddevice](https://python-sounddevice.readthedocs.io/)
- [NumPy](https://numpy.org/)
- [Pandas](https://pandas.pydata.org/)
- [PyTorch](https://pytorch.org/)
- [Joblib](https://joblib.readthedocs.io/)

## Installation

1. Clone the repository:

   ```bash
   git clone https://github.com/your-username/voice-controlled-robot.git

## Usage

1. Run virtual environment and install dependencies:
   pip install -r requirements.txt

2. streamlit run main.py

3. Open your web browser and go to http://localhost:8501 to use the application.

## Recording Voice Command

   - Click on the "Record Voice" button to start recording your voice command.
   - Speak into the microphone during the recording period.
   - The application will display a message when the recording is complete.

## Recognizing Voice Command

   - After recording, click on the "Recognize Voice Command" button.
   - The predicted command will be displayed in the application header.

## Model and Processing

   - The application uses a pre-trained neural network model to predict voice commands.
   - Audio data is processed using a transformer loaded from a saved model.

## Project Structure

- **main.py**: The main application file.
- **handler.py**: Module for extracting features from audio.
- **.gitignore**: File to ignore directories and files for the Git repository.
- **requirements.txt**: File with dependencies for the project.
- **README.md**: File with general information about the project.

### Models Directory

- **models/**: Directory with saved models and Scikit-Learn transformers.
  - **model_neural.pkl**: Pre-trained neural network model for voice command prediction.
  - **model_sklearn.joblib**: Saved Scikit-Learn model for any additional processing.
  - **processor.joblib**: Scikit-Learn transformer for preprocessing data.

### Notebooks Directory

- **notebooks/**: Directory containing Jupyter notebooks.
  - **features.ipynb**: Jupyter notebook for extracting features from audio.
  - **handler.py**: Module for extracting features from audio (also used in the main application).
  - **prediction.ipynb**: Jupyter notebook for the prediction process.
  - **pycache/**: Directory containing cached Python files.
    - **handler.cpython-310.pyc**: Cached file for the handler module.
  - **samples.ipynb**: Jupyter notebook with audio samples and testing.
