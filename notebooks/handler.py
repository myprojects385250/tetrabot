from librosa import feature as lbf # to extract features from audio files
import numpy as np # to manipulate arrays   

from sklearn.base import BaseEstimator, TransformerMixin # to create custom transformers    
from feature_engine.selection import DropDuplicateFeatures # to drop duplicate features
from feature_engine.selection import SmartCorrelatedSelection # to drop correlated features

# create a function to extract features from the audio
def get_features(y, sample_rate=24000, n_nfft=1024):
    '''
    the function extracts features from the audio signal
    y: audio signal
    sample_rate: sampling rate
    n_nfft: number of FFTs
    return: dictionary of features
    '''
    # extract spectral features
    # short-time Fourier transform
    chroma_stft = lbf.chroma_stft(y=y, sr=sample_rate, n_fft=n_nfft)
    # contstant-Q chromagram
    chroma_cqt = lbf.chroma_cqt(y=y, sr=sample_rate)
    # chroma energy normalized
    chroma_cens = lbf.chroma_cens(y=y, sr=sample_rate)
    # variable-Q chromagram
    chroma_vqt = lbf.chroma_vqt(y=y, sr=sample_rate, intervals='equal')
    # mel-scaled spectrogram
    melsp = lbf.melspectrogram(y=y, sr=sample_rate, n_fft=n_nfft)
    # mel-frequency cepstral coefficients
    mffc = lbf.mfcc(y=y, sr=sample_rate, n_fft=n_nfft)
    # root-mean-square value for each frame
    rmse = lbf.rms(y=y, frame_length=n_nfft, hop_length=n_nfft//2, center=True)
    # spectral centroid
    spec_cent = lbf.spectral_centroid(y=y, sr=sample_rate, n_fft=n_nfft, hop_length=n_nfft//2, center=True)
    # p'th-order spectral bandwidth
    spec_bw = lbf.spectral_bandwidth(y=y, sr=sample_rate, n_fft=n_nfft, hop_length=n_nfft//2, center=True, p=2)
    # spectral contrast
    spec_contrast = lbf.spectral_contrast(y=y, sr=sample_rate, n_fft=n_nfft, hop_length=n_nfft//2, center=True)
    # spectral flatness
    spec_flatness = lbf.spectral_flatness(y=y, n_fft=n_nfft, hop_length=n_nfft//2, center=True)
    # spectral roll-off frequency
    rolloff = lbf.spectral_rolloff(y=y, sr=sample_rate, n_fft=n_nfft, hop_length=n_nfft//2, center=True)
    # nth-order polynomial
    poly = lbf.poly_features(y=y, sr=sample_rate, n_fft=n_nfft, hop_length=n_nfft//2, center=True)
    # tonal centroid features
    tonnetz = lbf.tonnetz(y=y, sr=sample_rate)
    # zero-crossing rate
    zcr = lbf.zero_crossing_rate(y=y, frame_length=n_nfft, hop_length=n_nfft//2, center=True)

    # collect features in a dictionary
    features = {'stft': chroma_stft.mean(axis=1, keepdims=True),
                'cqt': chroma_cqt.mean(axis=1, keepdims=True),
                'cens': chroma_cens.mean(axis=1, keepdims=True),
                'vqt': chroma_vqt.mean(axis=1, keepdims=True),
                'melsp': melsp.mean(axis=1, keepdims=True),
                'mffc': mffc.mean(axis=1, keepdims=True),
                'rmse': rmse.mean(axis=1, keepdims=True),
                'spec_cent': spec_cent.mean(axis=1, keepdims=True),
                'spec_bw': spec_bw.mean(axis=1, keepdims=True),
                'spec_contrast': spec_contrast.mean(axis=1, keepdims=True),   
                'spec_flatness': spec_flatness.mean(axis=1, keepdims=True),
                'rolloff': rolloff.mean(axis=1, keepdims=True),
                'poly': poly.mean(axis=1, keepdims=True),
                'tonnetz': tonnetz.mean(axis=1, keepdims=True),
                'zcr': zcr.mean(axis=1, keepdims=True),
                }
    return features

# create a function to join all features in one vector
def stack_features(feat):
    '''
    the function joins all features in one vector
    feat: dictionary of features
    return: list of features
    '''
    features = None
    for v in feat.values():
        features = np.vstack((features, v)) if features is not None else v

    return features.T.reshape(-1).tolist()

# create a custom transformer to process features
class DropDupCorrScal(BaseEstimator, TransformerMixin):
    '''
    the class drops duplicate and correlated features
    '''
    def __init__(self, threshold=0.8):
        '''
        threshold: threshold for dropping correlated features
        '''
        self.threshold = threshold
        self.drop_dup = DropDuplicateFeatures(variables=None, missing_values='raise') # initialize the transformer for dropping duplicate features
        self.drop_corr = SmartCorrelatedSelection(variables=None, method='pearson',
                                                  threshold=threshold, missing_values='raise',
                                                  selection_method='variance') # initialize the transformer for dropping correlated features
    def fit(self, X, y=None):
        '''
        the function fits the transformer
        X: features
        y: target
        return: self
        '''
        self.drop_dup.fit(X) # fit the transformer for dropping duplicate features
        self.drop_corr.fit(X) # fit the transformer for dropping correlated features    
        return self

    def transform(self, X, y=None):
        '''
        the function transforms the features
        X: features
        y: target
        return: transformed features
        '''
 
        X = self.drop_dup.transform(X) # drop duplicate features
        X = self.drop_corr.transform(X) # drop correlated features
        return X