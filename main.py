import streamlit as st # to create a web app
import sounddevice as sd # to record audio
import numpy as np # to process the audio
import pandas as pd # to create a dataframe

import torch # to work with tensors
from notebooks.handler import get_features, stack_features # to extract features from the audio
import pickle # to save and load the model
from joblib import load # to save and load the model

def record_voice(duration=3, samplerate=24000):
    '''
    the function records the audio for a given duration and returns it as a numpy array
    duration: int, the duration of the recording in seconds
    samplerate: int, the sampling rate of the audio
    '''
    st.text("Recording... Please speak into the microphone.") # display a text while recording
    audio_data = sd.rec(int(samplerate * duration), samplerate=samplerate, channels=1, dtype=np.int16) # start recording
    sd.wait() # wait for the recording to finish
    st.text("Recording complete!") # display a text when the recording is complete
    return audio_data.flatten() # return the audio as a numpy array

def process_audio(y, samplerate=24000):
    '''
    the function takes the audio as a numpy array and returns the predicted command
    y: numpy array, the audio
    samplerate: int, the sampling rate of the audio
    '''

    commands_list = {0: 'move forward', 1: 'move backward', 2: 'stop', 3: 'turn to the left', 4: 'turn to the right'} # create a dictionary of commands and their number

    processor = load('models/processor.joblib') # load the transformer to preprocess the data
    with open('models/model_nueral.pkl', 'rb') as file: # load the model
        model_neural = pickle.load(file) # save the model to a variable
    st.text("Prediction... Please wait.") # display a text while predicting
        
    # create a dictionary of features and their number
    features_number = { key: value.shape[0] for key, value in get_features(y).items() }
    # create a list of columns names of each feature for the dataframe
    features_columns = [ key if value == 1 else key + '_' + str(i) for key, value in features_number.items() for i in range(value) ]

    # transform acquried data to a pandas dataframe
    X_data = np.array([stack_features(get_features(y))]).astype('float32')
    X_data = pd.DataFrame(X_data, columns=features_columns)

    sd.wait() # wait for the recording to process
    st.text("Prediction complete!") # display a text when the prediction is complete

    return commands_list[model_neural(torch.tensor(processor.transform(X_data).values)).argmax().item()] # return the predicted command

def main():
    '''
    the function creates a web app
    '''

    st.title("The application for voice control of the robot with Russian language") # display the title of the web app
    st.header("For now, the application can only recognize 5 commands: move forward, move backward, stop, turn to the left, turn to the right") # display the header of the web app

    if st.button("Record Voice"): # create a button to record the audio
        audio_data = np.array(record_voice(), dtype=np.float32) # record the audio
        st.audio(audio_data, sample_rate=24000, format="audio/wav") # display the audio
        
        st.header(f'Your command:  {process_audio(audio_data)}') # display the predicted command

if __name__ == "__main__":
    main()
